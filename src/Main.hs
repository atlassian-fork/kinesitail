{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}

module Main where

import Network.AWS
import Network.AWS.DynamoDB
import Network.AWS.Data.Text
import Network.AWS.Kinesis
import qualified Data.Text as T
import Data.String
import Control.Lens
import Control.Applicative
import qualified Data.HashMap.Strict as HM
import Data.Either.Combinators
import Data.Maybe
import qualified System.Environment as SE
import Text.Read
import Data.ByteString.Char8
import Control.Concurrent
import Data.Foldable
import Options.Applicative.Types
import Options.Applicative.Builder
import Options.Applicative.Extra
import Prelude hiding (putStrLn)

newtype UserbaseId = UserbaseId String
  deriving (Eq, Show)

instance IsString UserbaseId where
  fromString = UserbaseId

data EventType = ClientInserted

type DM = HM.HashMap Text AttributeValue


creds :: Credentials
creds =
  FromEnv "AWS_ACCESS_KEY_ID" "AWS_SECRET_ACCESS_KEY" (Just "AWS_SECURITY_TOKEN")

parseRegion :: String -> Maybe Region
parseRegion = rightToMaybe . fromText . fromString

explode :: String -> Maybe a -> IO a
explode f (Just r) = return r
explode f Nothing = fail f

getRegionFromEnv :: IO Region
getRegionFromEnv = do
  rs <- SE.getEnv "AWS_DEFAULT_REGION" :: IO String
  explode ("AWS_DEFAULT_REGION: " ++ rs ++ " does not identify a known region") (parseRegion rs)

getRegionFromEnvOrArgument :: Maybe String -> IO Region
getRegionFromEnvOrArgument = maybe getRegionFromEnv (\r -> return Sydney)

getAwsEnv :: KinesisStreamConfig -> IO Env
getAwsEnv config =  do
  r <- getRegionFromEnvOrArgument (streamEndpoint config)
  e <- maybe (newEnv r creds) (const (newEnv Sydney Discover)) (streamEndpoint config)
  s <- overrideWithEndpoint config kinesis
  return (configure s e)

overrideWithEndpoint :: KinesisStreamConfig -> Service -> IO Service
overrideWithEndpoint config service = return (maybe service (\host -> setEndpoint False (pack host) (fromMaybe 4567 (streamPort config)) service) (streamEndpoint config))

-- Kinesis Methods Start here

describe :: KinesisStreamConfig -> IO DescribeStreamResponse
describe config = run config (describeStream (T.pack (streamName config)))

run :: AWSRequest a => KinesisStreamConfig -> a -> IO (Rs a)
run config request = do
  e <- getAwsEnv config
  runResourceT (runAWS e (send request))

getShardLens :: Traversal' DescribeStreamResponse Shard
getShardLens = dsrsStreamDescription . sdShards . traverse

getRootShards :: Traversal' DescribeStreamResponse Shard
getRootShards = getShardLens . filtered (isNothing . view sParentShardId)

getChildShards :: Shard -> Traversal' DescribeStreamResponse Shard
getChildShards shard = getShardLens . filtered (\s -> fromMaybe False (view sParentShardId s <&> (== view sShardId shard)))

records :: KinesisStreamConfig -> Text -> IO GetRecordsResponse
records config shardId = run config (getRecords shardId)

iterator :: KinesisStreamConfig -> Shard -> IO GetShardIteratorResponse
iterator config shard = run config (getShardIterator (T.pack (streamName config)) (view sShardId shard) Latest)

getShardIteratorsLens :: Traversal' GetShardIteratorResponse Text
getShardIteratorsLens = gsirsShardIterator . traverse

printRecords :: GetRecordsResponse -> IO (Maybe Text)
printRecords response = do
    -- print response
    let records = view grrsRecords response
    case records of
      [] -> return ()
      r -> traverse (putStrLn . view rData) records *> return ()
    return (view grrsNextShardIterator response)

loop :: KinesisStreamConfig -> Shard -> Text -> IO ()
loop config currentShard currentShardIterator = do
    threadDelay 1000000
    response <- records config currentShardIterator
    nextShardIterator <- printRecords response
    fromMaybe (loopChildren config currentShard) (nextShardIterator <&> loop config currentShard)

loopChildren :: KinesisStreamConfig -> Shard -> IO ()
loopChildren config parentShard = do
  children <- describe config <&> toListOf (getChildShards parentShard)
  forkTail config children

forkTail :: KinesisStreamConfig -> [Shard] -> IO ()
forkTail config shards =
  let uncurriedLoop = uncurry (loop config) in do
    shardIterators <- traverse (\shard -> iterator config shard <&> view getShardIteratorsLens <&> (\shardIterator -> (shard, shardIterator))) shards
    -- print shardIterators
    case shardIterators of
      (h : t) -> traverse_ (forkIO . uncurriedLoop) t *> uncurriedLoop h
      [] -> return ()


data KinesisStreamConfig = KinesisStreamConfig
  { streamName :: String
  , streamEndpoint :: Maybe String
  , streamPort :: Maybe Int
  }

kinesisStreamConfig :: Parser KinesisStreamConfig
kinesisStreamConfig = KinesisStreamConfig
  <$> strOption (long "stream-name" <> short 'n' <> help "Kinesis Stream Name")
  <*> optional (strOption $ long "stream-endpoint" <> short 'e' <> help "Optional Kinesis Stream URL. Reads from Environment variables if not specified")
  <*> optional (option auto $ long "stream-port" <> short 'p' <> help "Optional Kinesis Stream Port. Default: 4567")


program config = do
  dsr <- describe config
  -- print dsr
  let rootShards = toListOf getRootShards dsr
  forkTail config rootShards

main :: IO ()
main = execParser opts >>= program
  where
    opts = info (helper <*> kinesisStreamConfig)
      ( fullDesc
        <> progDesc "Read from a Kinesis Stream. If a stream URL is not specified, the environment variable AWS_DEFAULT_REGION is expected, as well as AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY and AWS_SECURITY_TOKEN"
        <> header "" )
